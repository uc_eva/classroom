from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from classroom.apps.tracker.models import Attendance
from classroom.apps.account.models import Student
# Create your views here.


def attendance(request):

    if request.method=='GET':
        return render_to_response('attendance.html', context_instance=RequestContext(request))

def points(request):
    
    if request.method=='GET':
        
	return render_to_response('points.html', context_instance=RequestContext(request))

    if request.method=='POST':
        
	student_id= request.POST['student_id']
	behaviour_id= request.POST['behaviour_id']
        
	from classroom.apps.tracker.models import Behaviour
	behaviour_instance = Behaviour.objects.get(id=behaviour_id)
        behaviour_points = behaviour_instance.points
       
        from classroom.apps.account.models import Student
	student = Student.objects.get(student_id=student_id)
        
	# add or deduct the behaviour points
	student.points.value = student.points.value + behaviour_points
        student.save()
        return HttpResponse(content='points modified.')
