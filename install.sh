
# install python 2.7

DIR=`pwd`

mkdir ./packages/.envpython
cd ./packages


wget http://www.python.org/ftp/python/2.7.8/Python-2.7.8.tgz
tar -xvf Python-2.7.8.tgz
cd Python-2.7.8


./configure --with-zlib --prefix=$DIR/packages/.envpython --with-zlib
make
make install

cd ..


#wget http://www.zlib.net/zlib-1.2.3.tar.gz
#gzip -dc < zlib-1.2.3.tar.gz | tar -xf -
#cd zlib-1.2.3
#./configure -s --prefix=/usr
#make
#make install

wget http://pypi.python.org/packages/source/v/virtualenv/virtualenv-1.9.1.tar.gz
tar xzf virtualenv-1.9.1.tar.gz

#$DIR/packages/.envpython/bin/python virtualenv-1.9.1/setup.py install 

python virtualenv-1.9.1/virtualenv.py ../virtualenv 
#--python=$DIR/packages/.envpython/bin/python

cd ..

source ./virtualenv/bin/activate

pip install -r requirements.txt

deactivate

echo $DIR
