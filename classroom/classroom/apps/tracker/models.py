from mongoengine import * 
import datetime



# Create your models here.


class Attendance(Document):
    '''
        document to track the student attendance
    '''

    student = IntField()
    date = DateTimeField(default=datetime.datetime.now)
    


# Another way is to create a behaviour tuples,
# but then again, what if in feature it teacher has her own set of criteria

class Behaviour(Document):
    '''
        document to implement  the student behaviour
    '''
    id = IntField(primary_key=True)
    name = StringField(max_length=20)
    points = IntField()
    
#class Points(EmbeddedDocument):
    
#    value = IntField(default=100)
    

  
