from django.shortcuts import render
from django.http import HttpResponse
from classroom.apps.account.models import Student
from classroom.apps.account.models import Points

# Create your views here.


def studentRegistration(request):
    '''
        A simple student account creation. Note this is not the advisable way.
        Events related to posting to db is better to be handled via POST and 
	auth and auth. This is just a simple op to get things done.
    '''
    if request.method == 'GET':
  
       
       student_id = request.GET.get('student_id')
       name= request.GET.get('name')
       points = Points()
       student_instance = Student(student_id=student_id, name=name, age=21, points=points)
       student_instance.save() 
    
    return HttpResponse(content='creaed successfully')
