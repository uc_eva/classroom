This project helps you to track student attendance and also evaluate the student behaviour .
 
Technologies/Libraries to use
1: Django
2: Python
3: TastyPie
4: MongoDB



1: Create a bitbucket Account or use your existing one
2: Install Django Framework on your computer, For REST APIs, use TastyPie
3: Setup MongoDB
Create 4 model/tables in the DB
Table 1 : User Account ( student_id, Name, Age, Class)
Table 2:  Attendance ( id, student_id, Date, Time)
Table 3: Points: ( id, student_id, points)
Table 4: Behavior  ( id, Behaviour Name, Points )  { [1, Doing homework, 3],[2, Disrupting class, -2], [ 3, helping, 5] } 
4: Create an application that has 3 functions: Each function is represented as a URL that invokes the function.
URL1 : Creates a user account ( this can be just a simple username and age entered into the db) , this can be done by submitting a url which invokes the function to create an account. DO NOT CREATE A User Interface for this.
URL 2:  URL which updates the Attendance of the Student
URL 3:  This function should take the student name and behavior as input and update the Table 3 for the student.
5: Test this code out thoroughly using Unit Testing or Manual Testing

6: Upload the code to your bitcuket account

7: Upload all dependencies and packaged including version of django and python in this repository such that when any other use downloads the repo, they can execute a simple script and it should install everything and work


You will be judged by
1: Creative code
2: Efficient use of libraries
3: Scripting ability