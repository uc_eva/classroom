
from django.db import models
from mongoengine import *

#from classroom.apps.tracker.models import Points
# Create your models here.

class Points(EmbeddedDocument):
    value = IntField(default=100)


class Student(Document):
    '''

        Collection of student accounts.
    '''
    student_id = IntField(primary_key=True)
    name = StringField(max_length=120)
    age = IntField()
    grade = StringField(max_length=4) 
    points = EmbeddedDocumentField(Points)


 
