from tastypie_mongoengine import resources

from tastypie.authorization import Authorization
#from tastypie.serializers import Serializer
#from classroom.apps.account.models import Student


from tastypie.authentication import Authentication

from classroom.apps.tracker.models import Attendance

# The usual Authorization is failing and returning 401 UNAUTHORIZED
#class CustomAuthorization(Authorization):
#    def is_authorized(self, request, object=None):
#    	return True

#class CustomAuthentication(Authentication):
#    def is_authenticated(self, request, **kwargs):
#        return True

#class StudentResource(resources.MongoEngineResource):
 #   '''
#	Exposes a simple api to access the student Resource
#    '''
#    class Meta:
 #       queryset = Student.objects.all()
#        resource_name='student'
#        allowed_methods = ('get', 'post', 'put')
#        serializer = Serializer(formats=['json','xml'])
#        authentication = MultiAuthentication(Authentication(), BasicAuthentication(), ApiKeyAuthentication())
#        authorization=CustomAuthorization()


class AttendanceResource(resources.MongoEngineResource):
    '''
        Exposes a simple api to access the Attendance Resource
    '''

    class Meta:
        queryset = Attendance.objects.all()
        resource_name='attendance'
        allowed_methods= ('get', 'post', 'put')
	#authentication = Authentication()
	authorization = Authorization()





