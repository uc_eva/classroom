from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()


from tastypie.api import Api
from classroom.api.v1.resources import AttendanceResource

v1_api = Api(api_name='v1')
#v1_api.register(StudentResource())
v1_api.register(AttendanceResource())


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'classroom.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    #reqisterstudents ex:http://localhost:8000/register/?name=dude&student_id=10
    url(r'^register/$','classroom.apps.account.views.studentRegistration',name='studentRegistration'),
    
    #attendance uri ex: http://localhost:8000/attendance/?student_id=10
    url(r'^attendance_sheet/$','classroom.apps.tracker.views.attendance'),
    
    #points uri  http://localhost:8000/behaviour/

    url(r'^behaviour/$','classroom.apps.tracker.views.points'),
    
    # rest API. The following curl request 
   
    #curl --dump-header - -H "Content-Type: application/json" -X POST -H "X-CSRF
    #Token:y7kOsZzXoYyjAcIhNnMY0gU2rLgy3Yc9" --data "{\"student\": 1}" "http://l
    #ocalhost:8000/api/v1/attendance/"
    
     url(r'^api/',include(v1_api.urls)), 
    

    #url(r'^admin/', include(admin.site.urls)),
)
